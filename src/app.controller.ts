import { Controller, Get, Res, Req, HttpStatus } from '@nestjs/common';
import { AppService } from './app.service';
import { Response } from 'express';
import { resolve } from 'path';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  get(@Res() res: Response) {
    res.sendFile('index.html', {
      root: resolve('./dist'),
    });
  }

  @Get('api/weather')
   async getData(@Res() res, @Req() req) {
      const weatherData = await this.appService.getData(req.query);
      let uiWeather;
      if (weatherData && weatherData.cod === 200) {
        uiWeather = {
          dt: weatherData.dt,
          temp: (weatherData.main.temp_min + weatherData.main.temp_max) / 2,
          humidity: weatherData.main.humidity,
          name: weatherData.name,
          cod: weatherData.cod,
      }
    }

    return res.status(HttpStatus.OK).json(uiWeather ? uiWeather : weatherData);
  }
}
