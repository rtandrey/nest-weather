import { map } from 'rxjs/operators';
import { Injectable, HttpService } from '@nestjs/common';

@Injectable()
export class AppService {

  getHello(): string {
    return 'Hello World!';
  }
  constructor(private http: HttpService) {}

  getData(params): Promise<any> {
    const weatherUrl = `http://api.openweathermap.org/data/2.5/weather?lat=${params.lat}&units=metric&lon=${params.lon}&APPID=474489ef56d0cf49a9a28a2028f20a2c`;
    return this.http.get(weatherUrl).pipe(map(resp => resp.data)).toPromise();
  };
}
