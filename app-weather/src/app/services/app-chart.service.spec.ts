import { TestBed } from '@angular/core/testing';

import { AppChartService } from './app-chart.service';

describe('AppChartService', () => {
  let service: AppChartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppChartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
