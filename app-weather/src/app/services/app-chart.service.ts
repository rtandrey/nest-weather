import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppChartService {

  constructor(private http: HttpClient) { }

  rootURL = '/api';

  getWeather(location: {lon: string, lat: string}) {
    const params = {
      lon:  location.lon,
      lat: location.lat,
    };
    return this.http.get(this.rootURL + '/weather', {params});
  }
}
