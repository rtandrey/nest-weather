import { AppChartService } from './../../services/app-chart.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Label, Color } from 'ng2-charts';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { FormBuilder, FormGroup } from '@angular/forms';
import { interval, Subscription, combineLatest, forkJoin, TimeInterval } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';
import { get, map as _map } from 'lodash';
import * as moment from 'moment';

const locationKharkiv = { lon: 36.25, lat: 50 };

@Component({
  selector: 'app-chart',
  templateUrl: './app-chart.component.html',
  styleUrls: ['./app-chart.component.scss']
})
export class AppChartComponent implements OnInit {
  @ViewChild('myChart') myChart: ElementRef;

  constructor(
    private chartService: AppChartService,
    private fb: FormBuilder,
    ) { }

  weatherGroup: FormGroup;
  weatherSub: Subscription;
  city = '';
  isStart: boolean;
  infoMessage = '';

  temperatureData = [];
  humidityData = [];

  lonVal = 0;
  latVal = 0;

  lineChartData: ChartDataSets[] =
  [
    {
      label: 'Temperature',
      data: this.temperatureData,
    }, {
      label: 'Humidity ',
      yAxisID: 'y-axis-1',
      data: this.humidityData,
    }
  ];

  lineChartLabels: Label[] = [];

  lineChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          ticks: {
              suggestedMax: 100,
              suggestedMin: 0
            }
        }
      ]
    }
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';
  d3Data = [[], []];

  ngOnInit(): void {
    this.weatherGroup = this.fb.group({
      lon: locationKharkiv.lon,
      lat: locationKharkiv.lat,
    });
  }

  stopObservation() {
    this.isStart = false;
    if (this.weatherSub) {
      this.weatherSub.unsubscribe();
    }
  }

  startObservation() {
    const lonVal = this.weatherGroup.get('lon').value;
    const latVal = this.weatherGroup.get('lat').value;
    if (this.latVal !== latVal || this.lonVal !== lonVal) {
      this.clearCharts();
      this.latVal = latVal;
      this.lonVal = lonVal;
    }

    this.infoMessage = '';

    this.isStart = true;
    this.weatherSub = interval(3000).pipe(
      flatMap(() => this.chartService.getWeather({lon: lonVal, lat: latVal})))
      .subscribe(weatherData => {
          this.city = get(weatherData, 'name', 'N/A');
          this.setChartData(weatherData);
          this.infoMessage = get(weatherData, 'message', '');
        },
        err => {
          this.infoMessage = get(err, 'message', '');
          this.stopObservation();
        });
  }

  clearCharts() {
    this.lineChartData[0].data = [];
    this.lineChartData[1].data = [];
    this.lineChartLabels = [];
    this.d3Data = [[], []];
  }

  setChartData(weatherData) {
    const cod = get(weatherData, 'cod');
    if (cod !== 200) {
      this.stopObservation();
      return;
    }
    const temperature = get(weatherData, 'temp');
    const humidity = get(weatherData, 'humidity');
    this.lineChartData[0].data.push(temperature);
    this.lineChartData[1].data.push(humidity);

    this.d3Data = [
      _map(this.lineChartData[0].data, temp => ({data: temp})),
      _map(this.lineChartData[1].data, hum => ({data: hum})),
    ];
    this.lineChartLabels.push(moment.utc().local().format('YYYY-MM-DD HH:mm:ss'));
  }



}
