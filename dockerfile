FROM node:13
WORKDIR ./
COPY ./package.json ./
RUN npm install
COPY . .
RUN npm run build
EXPOSE 3000
WORKDIR ./app-weather
COPY ./package.json ./app-weather
RUN npm install
RUN npm run build
COPY . .
RUN cp -R dist ./app-weather
WORKDIR ./
CMD ["npm", "run", "start:prod"]